# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from .models import Friend, ClassYear
from .forms import Friendsform

def Index(request):
 
    return render(request,'mywebsite/Index.html')
    

def Portofolio(request):
	return render(request,'mywebsite/Portofolio.html')

def Form(request):
    data=Friend.objects.all()
    if request.method=="POST":
        form = Friendsform(request.POST)
        if form.is_valid():
            form.save()
    return render(request, 'mywebsite/Form.html',{'form':Friendsform,'data':data})