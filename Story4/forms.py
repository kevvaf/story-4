from django import forms
from .models import Friend, ClassYear

class Friendsform(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
    name = forms.CharField(
        label = 'Name',
        max_length = 30,
        #widget = forms.TextInput(attrs = attrs)
    )
    hobby = forms.CharField(
        label = 'Hobby',
        max_length = 30,
        #widget = forms.TextInput(attrs = attrs)
    )
    favorite = forms.CharField(
        label = 'Favorite',
        max_length = 30,
        #widget = forms.TextInput(attrs = attrs)
    )
    year = forms.ModelChoiceField(queryset=ClassYear.objects.all())