from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('',views.Index,name="Index"),
    path('Portofolio', views.Portofolio, name="Portofolio"),
    path('Connect', views.Form, name = "Form")
]