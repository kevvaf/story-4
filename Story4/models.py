from django.db import models

# Create your models here.

class ClassYear(models.Model):
    year = models.CharField(primary_key=True, max_length=20)
    def __str__(self):
        return f"{self.year}"


class Friend(models.Model):
    name = models.CharField(primary_key=True, max_length=20)
    hobby = models.CharField(max_length=20)
    favorite = models.CharField(max_length=20)
    year = models.ForeignKey(ClassYear, on_delete=models.CASCADE)
    def __str__(self):
        return f"{self.name}"   